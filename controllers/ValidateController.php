<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yz\shoppingcart\ShoppingCart;
use yii\web\NotFoundHttpException;
use app\modules\MubAdmin\modules\item\models\Category;
use app\modules\MubAdmin\modules\item\models\Subcat;
use app\modules\MubAdmin\modules\item\models\Brand;
use app\modules\MubAdmin\modules\item\models\Manufacture;

class ValidateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionCatagoryValidate($action,$id=null)
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new Category();
            $model->scenario = $action;
            if($action == 'create')
            {
                $model->deleteAll(['del_status' => '1']);
                $model->load(Yii::$app->request->post());
                return \yii\widgets\ActiveForm::validate($model);    
            }
            elseif($action == 'update')
            {
                $response = [];
                $model->load(Yii::$app->request->post());
                $modelEarlier = $model::findOne($id);
                if(($modelEarlier->name) !== ($model->name))
                {
                    $categoryModel = new Category();
                    $instances = $categoryModel::find()->where(['name' => $model->name])->count();
                    if($instances !== '0')
                    {
                        $response['category-name'] = ['Can not rename to already existing name'];
                        return $response;
                    }
                }
            }
            return $response;
        }
    }

    public function actionSubcatagoryValidate($action,$id=null)
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new Subcat();
            $model->scenario = $action;
            if($action == 'create')
            {
                $model->deleteAll(['del_status' => '1']);
                $model->load(Yii::$app->request->post());
                return \yii\widgets\ActiveForm::validate($model);
            }
            elseif($action == 'update')
            {
                $response = [];
                $model->load(Yii::$app->request->post());
                $modelEarlier = $model::findOne($id);
                if(($modelEarlier->name) !== ($model->name))
                {
                    $subcatModel = new Subcat();
                    $instances = $subcatModel::find()->where(['name' => $model->name])->count();
                    if($instances !== '0')
                    {
                        $response['subcat-name'] = ['Can not rename to already existing name'];
                        return $response;
                    }
                }
            }
            return $response;
        }
    }

    public function actionBrandValidate($action,$id=null)
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new Brand();
            $model->scenario = $action;
            if($action == 'create')
            {
                $model->deleteAll(['del_status' => '1']);
                $model->load(Yii::$app->request->post());
                return \yii\widgets\ActiveForm::validate($model);
            }
            elseif($action == 'update')
            {
                $response = [];
                $model->load(Yii::$app->request->post());
                $modelEarlier = $model::findOne($id);
                if(($modelEarlier->name) !== ($model->name))
                {
                    $brandModel = new Brand();
                    $instances = $brandModel::find()->where(['name' => $model->name])->count();
                    if($instances !== '0')
                    {
                        $response['brand-name'] = ['Can not rename to already existing name'];
                        return $response;
                    }
                }
            }
            return $response;
        }
    }

    public function actionManufactureValidate($action,$id=null)
    {
        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = new Manufacture();
            $model->scenario = $action;
            if($action == 'create')
            {
                $model->deleteAll(['del_status' => '1']);
                $model->load(Yii::$app->request->post());
                return \yii\widgets\ActiveForm::validate($model);
            }
            elseif($action == 'update')
            {
                $response = [];
                $model->load(Yii::$app->request->post());
                $modelEarlier = $model::findOne($id);
                if(($modelEarlier->name) !== ($model->name))
                {
                    $manufactureModel = new Manufacture();
                    $instances = $manufactureModel::find()->where(['name' => $model->name])->count();
                    if($instances !== '0')
                    {
                        $response['manufacture-name'] = ['Can not rename to already existing name'];
                        return $response;
                    }
                }
            }
        return $response;
        }
    }

}