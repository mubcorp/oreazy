<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yz\shoppingcart\ShoppingCart;
use yii\web\NotFoundHttpException;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\models\User;
use app\models\MubUser;
use app\models\MubUserContact;
use app\models\UserStates;
use yii\helpers\Url;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $this->layout = 'admin';
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionNewuser()
    {
        $mubUser = new MubUser(); 
        $mubUserContacts = new MubUserContact();
        $mubUserStates = new UserStates(); 
        $state = new \app\models\State();
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0','active' => '1'];
        $activeStates = $state->getAll('state_name',$where);
        $mubUser->scenario = 'create_mub_user';
        $mubUserContacts->scenario = 'create_mub_user';
        $mubUserStates->scenario = 'create_mub_user';

        return $this->render('newuser',[
            'mubUser' => $mubUser,
            'mubUserContacts' => $mubUserContacts,
            'mubUserStates' => $mubUserStates,'allStates' => $allStates,'activeStates' => $activeStates]);
    }

    public function actionProduct()
    {
        $category = new \app\modules\MubAdmin\modules\item\models\Category();
        $subcategory = new \app\modules\MubAdmin\modules\item\models\Subcat();
        $brand = new \app\modules\MubAdmin\modules\item\models\Brand();
        $where = ['del_status' => '0','status' => 'active'];
        $allcategory = $category->getAll('name',$where);
        $allsubcategory = $subcategory->getAll('name',$where);
        $allbrand = $brand->getAll('name',$where);
        $product = new \app\modules\MubAdmin\modules\item\models\Product();

        return $this->render('product',['product' => $product,'allcategory' => $allcategory,
            'allsubcategory' => $allsubcategory,
            'allbrand' => $allbrand,]);
    }

    public function actionSaleschannel()
    {
        return $this->render('saleschannel');
    }

}

