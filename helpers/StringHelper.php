<?php

namespace app\helpers;

class StringHelper
{
	public static function generateSlug($string)
	{
		$string = trim($string);
		$string = str_replace(' ', '-', $string);
		$data = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
		return strtolower($data);
	}

	public function getFistLastName($name)
	{
		$clientName	= explode(' ',$name);
		$response = [];
		if(count($clientName) > 1)
		{
			$response['first_name'] = $clientName[0];
			$response['last_name'] = $clientName[1];
		}
		else
		{
			$response['first_name'] = $clientName[0];
			$response['last_name'] = $clientName[0];
		}
		return $response;
	}
}