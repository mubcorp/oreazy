<?php

/* @var $this \yii\web\View */
/* @var $content string */



use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Oreazy | Home :: MakeUBIG</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basic Trade Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--css links-->
<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /><!--bootstrap-->
<link href="/css/font-awesome.css" rel="stylesheet"><!--font-awesome-->
<link rel="stylesheet" href="/css/chocolat.css" type="text/css" media="screen"><!--chocolat-->
<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" /><!--stylesheet-->
<!--//css links-->
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700" rel="stylesheet">
<!--//fonts-->
</head>
<body>
<!-- Header -->
<div id="home" class="banner w3l">
        <div class="header-nav">
            <nav class="navbar navbar-default">
                    <div class="navbar-header logo">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <h1>
                                    <a class="navbar-brand" href="/">Basic trade</a>
                                </h1>
                    </div>
                    <!-- navbar-header -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/" class="hvr-bounce-to-bottom active">Home</a></li>
                            <li><a href="/" class="hvr-bounce-to-bottom scroll">About</a></li>
                            <li><a href="#services" class="hvr-bounce-to-bottom scroll">Services</a></li>
                            <li><a href="#projects" class="hvr-bounce-to-bottom scroll">Projects</a></li>
                            <li><a href="#team" class="hvr-bounce-to-bottom scroll">Team</a></li>
                            <li><a href="#contact" class="hvr-bounce-to-bottom scroll">Contact</a></li>
                        </ul>
                    </div>
                    <div class="contact-bnr-w3-agile">
                                <ul>
                                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i>+1 (100)222-0-33</li> 
                                </ul>
                            </div>
                    <div class="clearfix"> </div>   
                </nav>
        </div>
          
</div>  
<?= $content ?>
    <div class="contact" id="contact">
                    <div class="col-md-5 mail-grid1-form ">
                        <h3 class="tittle-w3"><span>Send a </span>Message</h3>
                        <form action="#" method="post">
                            <input type="text" name="Name" placeholder="Name" required="" />
                            <input type="email" name="Email" placeholder="Email" required="" />
                            <textarea name="Message" placeholder="Type Your Text Here...." required=""></textarea>
                            <input type="submit" value="Submit">
                        </form>
                    </div>
                <div class="col-md-7 map ">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26359195.17562375!2d-113.7156245614499!3d36.2473834534249!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1452332634941"></iframe>
                </div>
                    <div class="clearfix"></div>
                </div>
<!-- //mail -->
<!-- footer -->
<div class="footer w3layouts">
        <div class="container">
            <div class="footer-row w3layouts-agile">
                <div class="col-md-4 footer-grids w3l-agileits">
                    <h3>About Us</h3>
                    <p class="footer-one-w3ls">This is a great space to write long text about your company and your services. </p>
                    <div class="social-icons-agileits">
                        <ul>
                            <li><a class="hvr-bounce-to-bottom" href="#"><i class="fa fa-facebook f1" aria-hidden="true"></i></a></li>
                            <li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-twitter f2" aria-hidden="true"></i></a></li>
                            <li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-google-plus f3" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 footer-grids w3l-agileits">
                    <h3>Navigation</h3>                 
                    <ul class="b-nav">
                        <li><a href="#home" class="scroll">Home</a></li>
                        <li><a href="#about" class="scroll">About us</a></li>
                        <li><a href="#team" class="scroll">Team</a></li>
                        <li><a href="#services" class="scroll">Services</a></li>
                    </ul>
                </div>
                <div class="col-md-2 footer-grids w3l-agileits">
                    <h3>Support</h3>
                    <ul class="b-nav">
                        <li><a href="#projects" class="scroll">Latest projects</a></li>
                        <li><a href="#testimonials" class="scroll">Clients</a></li>
                        <li><a href="#home" class="scroll">Lemollisollis</a></li>
                    </ul>
                </div>
                <div class="col-md-3 footer-grids w3l-agileits">    
                    <h3>Newsletter</h3>
                    <p>It was popularised in the 1960s with the release Ipsum. </p>
                     <form action="#" method="post">         
                            <input type="email" placeholder="Email" name="email" required="" />
                            <input type="submit" value="Subscribe" />                    
                     </form>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!-- //footer -->
<div class="copy">
    <div class="container">
        <h2><a href="index.html"><span>B</span>asic <span>T</span>rade</a></h2>
        <p>© 2017 Basic Trade . All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
    </div>
</div>
<!--/footer -->
<!-- js -->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<!--//js -->
<!-- responsiveslides -->
<script src="/js/responsiveslides.min.js"></script>
            <script>
                                    // You can also use "$(window).load(function() {"
                                    $(function () {
                                     // Slideshow 4
                                    $("#slider3").responsiveSlides({
                                        auto: true,
                                        pager: true,
                                        nav: true,
                                        speed: 500,
                                        namespace: "callbacks",
                                        before: function () {
                                    $('.events').append("<li>before event fired.</li>");
                                    },
                                    after: function () {
                                        $('.events').append("<li>after event fired.</li>");
                                        }
                                        });
                                        });
            </script>
<!-- //responsiveslides -->
<!-- OnScroll-Number-Increase-JavaScript -->
    <script type="text/javascript" src="/js/numscroller-1.0.js"></script>
<!-- //OnScroll-Number-Increase-JavaScript -->
<!--Scrolling-top -->
<script type="text/javascript" src="/js/move-top.js"></script>
<script type="text/javascript" src="/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!--//Scrolling-top -->
<!--light-box-files -->
        <script src="/js/modernizr.custom.js"></script>
        <script src="/js/jquery.chocolat.js"></script>
<!-- PopUp-Box-JavaScript -->
        <script type="text/javascript">
            $(function() {
            $('.filtr-item a').Chocolat();
            });
        </script>
<!-- //PopUp-Box-JavaScript -->
 <!--light-box-files -->
 <!--flexiselDemo1 -->
 <script type="text/javascript">
                            $(window).load(function() {
                                $("#flexiselDemo1").flexisel({
                                    visibleItems: 2,
                                    animationSpeed: 1000,
                                    autoPlay: true,
                                    autoPlaySpeed: 3000,            
                                    pauseOnHover: true,
                                    enableResponsiveBreakpoints: true,
                                    responsiveBreakpoints: { 
                                        portrait: { 
                                            changePoint:480,
                                            visibleItems: 1
                                        }, 
                                        landscape: { 
                                            changePoint:640,
                                            visibleItems: 1
                                        },
                                        tablet: { 
                                            changePoint:991,
                                            visibleItems: 1
                                        }
                                    }
                                });
                                
                            });
            </script>
            <script type="text/javascript" src="/js/jquery.flexisel.js"></script>
<!--//flexiselDemo1 -->
<!-- smooth scrolling -->
    <script type="text/javascript">
        $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
        */                              
        $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="/js/bootstrap-3.1.1.min.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
