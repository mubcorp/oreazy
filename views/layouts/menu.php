<div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="/" class="simple-text">
                Oreazy ADMIN
            </a>
        </div>

        <ul class="nav">
            <li <?php if($currnetClass == 'dashboard'){ ?> class="active" <?php } ?> >
                <a href="/mub-admin">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li <?php if($currnetClass == 'user'){ ?> class="active" <?php } ?>>
                <a href="/mub-admin/users">
                    <i class="ti-user"></i>
                    <p>Users</p>
                </a>
            </li>
            <li <?php if($currnetClass == 'product'){ ?> class="active" <?php } ?>>
                <a href="/mub-admin/item/product">
                    <i class="ti-view-list-alt"></i>
                    <p>Product</p>
                </a>
            </li>
            <li <?php if($currnetClass == 'category'){ ?> class="active" <?php } ?>>
                <a href="/mub-admin/item/category">
                    <i class="ti-text"></i>
                    <p>Category</p>
                </a>
            </li>
            <li <?php if($currnetClass == 'subcat'){ ?> class="active" <?php } ?>>
                <a href="/mub-admin/item/subcat">
                    <i class="ti-map"></i>
                    <p>Sub-Category</p>
                </a>
            </li>
            <li <?php if($currnetClass == 'manufacture'){ ?> class="active" <?php } ?>>
                <a href="/mub-admin/item/manufacture">
                    <i class="ti-pencil-alt2"></i>
                    <p>Manufacture</p>
                </a>
            </li>
            <li <?php if($currnetClass == 'brand'){ ?> class="active" <?php } ?>>
                <a href="/mub-admin/item/brand">
                    <i class="ti-bell"></i>
                    <p>Brand</p>
                </a>
            </li>
            <li class="active-pro" <?php if($currnetClass == 'dashboard'){ ?> class="active" <?php } ?>>
                <a href="/mub-admin">
                    <i class="ti-export"></i>
                    <p>Profile</p>
                </a>
            </li>
        </ul>
    </div>
</div>