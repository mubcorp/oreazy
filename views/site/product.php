<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\ImageUploader;
use dosamigos\fileupload\FileUploadUI;
use app\modules\MubAdmin\modules\item\models\ProductImages;
$productImages = new ProductImages();

/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\item\products\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Oreazy | Home :: MakeUBIG</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basic Trade Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--css links-->
<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /><!--bootstrap-->
<link href="/css/font-awesome.css" rel="stylesheet"><!--font-awesome-->
<link rel="stylesheet" href="/css/chocolat.css" type="text/css" media="screen"><!--chocolat-->
<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" /><!--stylesheet-->
<!--//css links-->
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700" rel="stylesheet">
<!--//fonts-->
</head>
<body>
<!-- Header -->

<div class="container" style="margin-top: 4em; margin-bottom: 5em;">
<div class="product-form">
    <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
      <h1>Add Product</h1>
      <br/>
      
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6"><?= $form->field($product, 'model_number')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'item_package_quantity')->dropDownList([ 'quantity' => 'Quantity', 'quantity2' => 'Quantity2', ], ['prompt' => '']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'size')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'size_map')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'color')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'color_map')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'enclosure_material')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'manufacturer')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'title')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'manufacturer_part_number')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'brand')->textInput() ?></div>

    <div class="col-md-6"><?php echo $form->field($product, 'category')->dropDownList($allcategory,['class' => 'border-input form-control'])->label('CATEGORY'); ?>
    </div>
    <div class="col-md-6"><?php echo $form->field($product, 'sub_category')->dropDownList([],['class' => 'border-input form-control'])->label('SUB CATEGORY'); ?>
    </div>

    <div class="col-md-6"><?= $form->field($product, 'seller_sku')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'HSN_code')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'price')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'sale_price')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'warranty_des')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'country')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'release_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'sale_start_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'sale_end_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'restock_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'offer_release_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'condition_notes')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'leagle_notes')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'quantity')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'max_order_quantity')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'product_text_code')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'condition')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'handling_time')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'gift_wrap')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ], ['prompt' => '']) ?></div>

    <div class="col-md-12"><?= $form->field($product, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?></div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-lg']) ?></div>
    </div>

</div>
</div>
    <?php ActiveForm::end(); ?>

</div>

<!--/footer -->
<!-- js -->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<!--//js -->
<!-- responsiveslides -->
<script src="/js/responsiveslides.min.js"></script>
            <script>
                                    // You can also use "$(window).load(function() {"
                                    $(function () {
                                     // Slideshow 4
                                    $("#slider3").responsiveSlides({
                                        auto: true,
                                        pager: true,
                                        nav: true,
                                        speed: 500,
                                        namespace: "callbacks",
                                        before: function () {
                                    $('.events').append("<li>before event fired.</li>");
                                    },
                                    after: function () {
                                        $('.events').append("<li>after event fired.</li>");
                                        }
                                        });
                                        });
            </script>
<!-- //responsiveslides -->
<!-- OnScroll-Number-Increase-JavaScript -->
    <script type="text/javascript" src="/js/numscroller-1.0.js"></script>
<!-- //OnScroll-Number-Increase-JavaScript -->
<!--Scrolling-top -->
<script type="text/javascript" src="/js/move-top.js"></script>
<script type="text/javascript" src="/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!--//Scrolling-top -->
<!--light-box-files -->
        <script src="/js/modernizr.custom.js"></script>
        <script src="/js/jquery.chocolat.js"></script>
<!-- PopUp-Box-JavaScript -->
        <script type="text/javascript">
            $(function() {
            $('.filtr-item a').Chocolat();
            });
        </script>
<!-- //PopUp-Box-JavaScript -->
 <!--light-box-files -->
 <!--flexiselDemo1 -->
 <script type="text/javascript">
                            $(window).load(function() {
                                $("#flexiselDemo1").flexisel({
                                    visibleItems: 2,
                                    animationSpeed: 1000,
                                    autoPlay: true,
                                    autoPlaySpeed: 3000,            
                                    pauseOnHover: true,
                                    enableResponsiveBreakpoints: true,
                                    responsiveBreakpoints: { 
                                        portrait: { 
                                            changePoint:480,
                                            visibleItems: 1
                                        }, 
                                        landscape: { 
                                            changePoint:640,
                                            visibleItems: 1
                                        },
                                        tablet: { 
                                            changePoint:991,
                                            visibleItems: 1
                                        }
                                    }
                                });
                                
                            });
            </script>
            <script type="text/javascript" src="/js/jquery.flexisel.js"></script>
<!--//flexiselDemo1 -->
<!-- smooth scrolling -->
    <script type="text/javascript">
        $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
        */                              
        $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="/js/bootstrap-3.1.1.min.js"></script>
