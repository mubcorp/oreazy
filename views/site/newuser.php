
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\City;
if(Yii::$app->controller->action->id == 'update')
{
    $city = $mubUserContacts->city;
    $cityModel = new City();
    $state = $cityModel::findOne($city)->state_id;
    $allCities = $cityModel->getManyById($state,'state_id','city_name');
    $status = $mubUser->status;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Oreazy | Home :: MakeUBIG</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basic Trade Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!--css links-->
<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /><!--bootstrap-->
<link href="/css/font-awesome.css" rel="stylesheet"><!--font-awesome-->
<link rel="stylesheet" href="/css/chocolat.css" type="text/css" media="screen"><!--chocolat-->
<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" /><!--stylesheet-->
<!--//css links-->
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700" rel="stylesheet">
<!--//fonts-->
</head>
<body>


<div class="container" style="margin-top: 4em; margin-bottom: 5em;">
<div class="product-form">

<div class="mub-user-form">
<div class="col-md-1"></div>
      <h1>Create your Account</h1>
      <br/>
      
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">

        <?= $form->field($mubUser, 'first_name')->textInput(['maxlength' => true]) ?>
            
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($mubUser, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">

        <?= $form->field($mubUserContacts, 'mobile')->textInput(['maxlength' => '10']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">

        <?= $form->field($mubUserContacts, 'address')->textInput(['maxlength' => true])?>

        </div>
    </div>
 
    <div class="row">
        <?php 
            if(Yii::$app->controller->action->id == 'update')
        {?>
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($mubUserContacts, 'state')->dropDownList($allStates,['options' =>
                        [                        
                          $state => ['selected' => true]
                        ]
              ],['id' => 'mub-state','prompt' => 'Select A state']);?>
       </div>
        <div class="col-md-5 col-sm-12 col-xs-12">

            <?= $form->field($mubUserContacts, 'city')->dropDownList($allCities, ['prompt' => 'Select A City']);?>
        </div>
        <?php }
        else
        { ?>
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($mubUserContacts, 'state')->dropDownList($allStates, ['id' => 'mub-state','prompt' => ' Select A state']);?>
        </div>

        <div class="col-md-5 col-sm-12 col-xs-12">
            <?= $form->field($mubUserContacts, 'city')->dropDownList([], ['prompt' => 'Select A City']);?>
        </div>
        <?php }
        ?>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($mubUser, 'organization')->textInput()->label('Company Name');?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($mubUser, 'domain')->textInput();?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
          <?= $form->field($mubUser, 'username')->textInput(['maxlength' => true, 'readonly' => (Yii::$app->controller->action->id == 'update') ? true : false]);?>
          </div>
          <?php if(Yii::$app->controller->action->id == 'update'){
        ?>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($mubUser, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'InActive'], ['prompt' => 'Select A Status']);?>
        </div>
        <?php }else{?>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($mubUser, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'InActive'],['prompt' => 'Select User Status']);?>
        </div>
        <?php }?>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($mubUser, 'password')->passwordInput(['maxlength' => true]);?>
            
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
            <?= $form->field($mubUser, 'password_confirm')->passwordInput(['maxlength' => true]);?>
        </div>
        <?php ?>
    </div>
    <div class="row">

        <div class="form-group">
            <div class="col-md-12" style="text-align: center;" >
                <?= Html::submitButton($mubUser->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $mubUser->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
<!--/footer -->
<!-- js -->
<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
<!--//js -->
<!-- responsiveslides -->
<script src="/js/responsiveslides.min.js"></script>
            <script>
                                    // You can also use "$(window).load(function() {"
                                    $(function () {
                                     // Slideshow 4
                                    $("#slider3").responsiveSlides({
                                        auto: true,
                                        pager: true,
                                        nav: true,
                                        speed: 500,
                                        namespace: "callbacks",
                                        before: function () {
                                    $('.events').append("<li>before event fired.</li>");
                                    },
                                    after: function () {
                                        $('.events').append("<li>after event fired.</li>");
                                        }
                                        });
                                        });
            </script>
<!-- //responsiveslides -->
<!-- OnScroll-Number-Increase-JavaScript -->
    <script type="text/javascript" src="/js/numscroller-1.0.js"></script>
<!-- //OnScroll-Number-Increase-JavaScript -->
<!--Scrolling-top -->
<script type="text/javascript" src="/js/move-top.js"></script>
<script type="text/javascript" src="/js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!--//Scrolling-top -->
<!--light-box-files -->
        <script src="/js/modernizr.custom.js"></script>
        <script src="/js/jquery.chocolat.js"></script>
<!-- PopUp-Box-JavaScript -->
        <script type="text/javascript">
            $(function() {
            $('.filtr-item a').Chocolat();
            });
        </script>
<!-- //PopUp-Box-JavaScript -->
 <!--light-box-files -->
 <!--flexiselDemo1 -->
 <script type="text/javascript">
                            $(window).load(function() {
                                $("#flexiselDemo1").flexisel({
                                    visibleItems: 2,
                                    animationSpeed: 1000,
                                    autoPlay: true,
                                    autoPlaySpeed: 3000,            
                                    pauseOnHover: true,
                                    enableResponsiveBreakpoints: true,
                                    responsiveBreakpoints: { 
                                        portrait: { 
                                            changePoint:480,
                                            visibleItems: 1
                                        }, 
                                        landscape: { 
                                            changePoint:640,
                                            visibleItems: 1
                                        },
                                        tablet: { 
                                            changePoint:991,
                                            visibleItems: 1
                                        }
                                    }
                                });
                                
                            });
            </script>
            <script type="text/javascript" src="/js/jquery.flexisel.js"></script>
<!--//flexiselDemo1 -->
<!-- smooth scrolling -->
    <script type="text/javascript">
        $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
        */                              
        $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="/js/bootstrap-3.1.1.min.js"></script>
