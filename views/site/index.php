<div class="about" id="about">
		<div class="container">
		<!-- <h3 class="heading-agileinfo">About us</h3> -->
			<div class="col-md-12 banner_bottom_right">
				<div class="wthree_banner_bottom_right_grids">
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<a href="/site/newuser"><h3>Create Account</h3></a>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="iages/a1.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<a href="/site/product"><h3>Add Product</h3></a>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="imges/a2.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<a href="/site/saleschannel"><h3>Sales Channel</h3></a>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="image/a2.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<h3>Your Profile</h3>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="image/a2.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="wthree_banner_bottom_right_grids">
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<h3>Create Account</h3>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="image/a3.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<h3>Create Account</h3>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="image/a2.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<h3>Product List</h3>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="image/a2.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="col-md-3 banner_bottom_right_grid">
						<div class="view view-tenth">
							<div class="agile_text_box">
								<a href="/mub-admin" target="_blank"><h3>Go to Dashboard</h3></a>
								<p>Lorem ipsum dolor sit </p>
							</div>
							<div class="mask">
								<img src="image/a4.jpg" class="img-responsive" alt="" />
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>

			</div>
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- banner-bottom -->
<!-- Modal2 -->
						<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4>Basic Trade</h4>
										<img src="images/g5.jpg" alt=" " class="img-responsive">
										<h5>Creating a business with authentic integrity</h5>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting.</p>
									</div>
								</div>
							</div>
						</div>
<!-- //Modal2 -->
<!-- Stats -->
	<div class="stats-agileits" id="stats">

		<h3 class="heading-agileinfo">We Have Something To Be Proud Of</h3>

		<div class="stats-info agileits w3layouts">
			<div class="col-md-6 agileits w3layouts col-sm-6 stats-grid stats-grid-1">
				<div class="numscroller agileits-w3layouts" data-slno='1' data-min='0' data-max='2500' data-delay='3' data-increment="2">2500</div>
				<div class="stat-info-w3ls">
					<i class="fa fa-users" aria-hidden="true"></i>
					<h4 class="agileits w3layouts">Team Members</h4>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 agileits w3layouts stats-grid stats-grid-2">
				<div class="numscroller agileits-w3layouts" data-slno='1' data-min='0' data-max='3421' data-delay='3' data-increment="2">3421</div>
				<div class="stat-info-w3ls">
					<i class="fa fa-user-plus" aria-hidden="true"></i>
					<h4 class="agileits w3layouts">Satisfied clients</h4>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 stats-grid agileits w3layouts stats-grid-3">
				<div class="numscroller agileits-w3layouts" data-slno='1' data-min='0' data-max='15' data-delay='3' data-increment="2">15</div>
				<div class="stat-info-w3ls">
					<i class="fa fa-line-chart" aria-hidden="true"></i>
					<h4 class="agileits w3layouts">Years in the industry</h4>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 stats-grid stats-grid-4 agileits w3layouts">
				<div class="numscroller agileits-w3layouts" data-slno='1' data-min='0' data-max='450' data-delay='3' data-increment="2">450</div>
				<div class="stat-info-w3ls">
					<i class="fa fa-check-square-o" aria-hidden="true"></i>
					<h4 class="agileits w3layouts">Projects Done</h4>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //Stats -->
<!--Services --> 
		<div class="services" id="services">
		<h3 class="heading-agileinfo">What we do</h3>
				<div class="popular-grids">
					<div class="col-md-3 popular-grid">
						<img src="images/a1.jpg" class="img-responsive" alt=""/>
						<div class="popular-text">
							<i class="glyphicon glyphicon-user" aria-hidden="true"></i>
							<h5>Developing</h5>
							<div class="detail-bottom">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore sed do eiusmod tempor incididunt ut labore</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 popular-grid">
						<img src="images/a2.jpg" class="img-responsive" alt=""/>
						<div class="popular-text">
							<i class="glyphicon glyphicon-arrow-up" aria-hidden="true"></i>
							<h5>Marketing</h5>
							<div class="detail-bottom">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore sed do eiusmod tempor incididunt ut labore</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 popular-grid">
						<img src="images/a3.jpg" class="img-responsive" alt=""/>
						<div class="popular-text">
							<i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>
							<h5>Analytics</h5>
							<div class="detail-bottom">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore sed do eiusmod tempor incididunt ut labore</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 popular-grid">
						<img src="images/a4.jpg" class="img-responsive" alt=""/>
						<div class="popular-text">
							<i class="glyphicon glyphicon-usd" aria-hidden="true"></i>
							<h5>Support</h5>
							<div class="detail-bottom">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore sed do eiusmod tempor incididunt ut labore</p>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
		</div>
<!-- //services --> 
<!-- Team -->
<div class="team" id="team">
	<div class="container">
		<h3 class="heading-agileinfo">Meet Our Team</h3>
			<div class="team-main">
			<div class="team-bottom">
				<div class="col-md-3 team-grid">
					<img src="images/t1.jpg" alt="" >
					<div class="team-text">
						<h4>Michael Lii</h4>
						<h5>CEO</h5>
						<p>Eiusmod tempor incididunt ut labore.</p>
					</div>
					<div class="caption">
						<ul>
							<li><a class="hvr-bounce-to-bottom" href="#"><i class="fa fa-facebook f1" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-twitter f2" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-google-plus f3" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 team-grid">
					<img src="images/t2.jpg" alt="" >
					<div class="team-text">
						<h4>Max Payne</h4>
						<h5>Manager</h5>
						<p>Eiusmod tempor incididunt ut labore.</p>
					</div>
					<div class="caption">
						<ul>
							<li><a class="hvr-bounce-to-bottom" href="#"><i class="fa fa-facebook f1" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-twitter f2" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-google-plus f3" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 team-grid">
					<img src="images/t3.jpg" alt="" >
					<div class="team-text">
						<h4>Rosy Smith</h4>
						<h5>Employee</h5>
						<p>Eiusmod tempor incididunt ut labore.</p>
					</div>
					<div class="caption">
						<ul>
							<li><a class="hvr-bounce-to-bottom" href="#"><i class="fa fa-facebook f1" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-twitter f2" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-google-plus f3" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 team-grid">
					<img src="images/t4.jpg" alt="" >
					<div class="team-text">
						<h4>Williams Allen</h4>
						<h5>Manager</h5>
						<p>Eiusmod tempor incididunt ut labore.</p>
					</div>
					<div class="caption">
						<ul>
							<li><a class="hvr-bounce-to-bottom" href="#"><i class="fa fa-facebook f1" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-twitter f2" aria-hidden="true"></i></a></li>
							<li><a class="hvr-bounce-to-bottom" href="#" ><i class="fa fa-google-plus f3" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	  
    </div>
</div>	
<!-- /Team -->
<!-- Portfolio -->
	<div class="portfolio w3layouts agileits" id="projects">
	<div class="container">
			<h3 class="heading-agileinfo">Latest Projects</h3>
			
			<div class="filtr-container w3layouts agileits">

				<div class="filtr-item w3layouts agileits portfolio-t">
					<a href="images/g1.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g1.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits">
					<a href="images/g2.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g2.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits">
					<a href="images/g3.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g3.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits">
					<a href="images/g4.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g4.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits" data-category="3, 4" data-sort="Industrial site">
					<a href="images/g5.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g5.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits">
					<a href="images/g6.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g6.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits">
					<a href="images/g7.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g7.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits">
					<a href="images/g8.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g8.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="filtr-item w3layouts agileits">
					<a href="images/g9.jpg" class="b-link-stripe w3layouts agileits b-animate-go thickbox">
						<figure>
							<img src="images/g9.jpg" class="img-responsive w3layouts agileits" alt="W3layouts Agileits">
							<figcaption>
								<h3><span>B</span>asic <span>T</span>rade</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</figcaption>
						</figure>
					</a>
				</div>
				<div class="clearfix"></div>
			</div>
	</div>
</div>
	<!-- //Portfolio -->
<!--testimonials-->
<div id="testimonials" class="testimonials">
	<div class="container">
		<h3 class="heading-agileinfo">1000+ Happy Clients</h3>
		<div class="flex-slider">
			<ul id="flexiselDemo1">			
				<li>
					<div class="laptop">
						<div class="col-md-8 team-right">
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
								adipisci velit, sed quia non numquam.</p>
							<div class="name-w3ls">
								<h5>Federica</h5>
								<span>lorem ipsum</span>
							</div>
						</div>
						<div class="col-md-4 team-left">
							<img class="img-responsive" src="images/test1.jpg" alt=" " />
						</div>
						<div class="clearfix"></div>
					</div>
				</li>
				<li>
					<div class="laptop">
						<div class="col-md-8 team-right">
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
								adipisci velit, sed quia non numquam.</p>
							<div class="name-w3ls">
								<h5>Thompson</h5>
								<span>lorem ipsum</span>
							</div>
						</div>
						<div class="col-md-4 team-left">
							<img class="img-responsive" src="images/test2.jpg" alt=" " />
						</div>
						<div class="clearfix"></div>
					</div>
				</li>
			</ul>
			
		</div>

	</div>
</div>