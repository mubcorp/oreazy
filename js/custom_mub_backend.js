$(document).ready(function() {
   $('#product-image').hide();
});
$(document).ready(function(){
  var droplist = $('#show_value');
  droplist.change(function(e){
    if (droplist.val() == 'item') {
      $('#product-image').show();
    }
    else {
      $('#product-image').hide();
    }
  })
});

$('#submit_product').on('click',function(){
  $('#oreazy_item').submit();
});

$(document).ready(function(){
  var droplist = $('#show_value');
  droplist.change(function(e){
    if (droplist.val() == 'planning') {
      $('#mydiv1').show();
    }
    else {
      $('#mydiv1').hide();
    }
  })
});

 var setModelAttribute = function(model,attribute,value,id)
  {
      var dataArray = {model:model,attribute:attribute,value:value,id:id};
      $.ajax({
       type:'POST',
       url:"/mub-admin/dashboard/set-attribute",
       data:dataArray,
          success: function(result){
              if(result)
              {
                  location.reload();
              }
          }
      }); 
  };

  var changeUserStatus = function(field,val,id)
    {
        var positive = confirm('Are you sure you want to change this user\'s status?');
        if(positive)
        {
          var models = [{name:'\\app\\models\\User',status:val},{name:'\\app\\models\\MubUser',status:val}];
          for (var i = models.length - 1; i >= 0; i--) {
            setModelAttribute(models[i].name,'status',val,id);  
          }
        }
    };

  function getFormattedPartTime(partTime){
     if (partTime < 10)
        return "0"+partTime;
     return partTime;
  }

	$('#product-category').on('change',function(){
    var selected = $("#product-category option:selected").val();
    $.ajax({
         type:'POST',
         url:"/mub-admin/dashboard/get-sub-category",
         data:{categoryId:selected},
        success: function(result){
            $('#product-sub_category').children("option").remove();
            $('#product-sub_category').prop("disabled", false);
            $('#product-sub_category').append(new Option('Select', ''));
        $.each(result.result, function (i, item) {
          $('#product-sub_category').append(new Option(item.name, item.id));
        });
        }
      })
    }); 
  
   $('#bed-available').on('change',function(){
       var selected = $("#bed-available option:selected").val();
       if(selected == 1)
       {
           var date = new Date();
           var month = date.getMonth()+1;
           var str = date.getFullYear() + "-" + getFormattedPartTime(month) + "-" + getFormattedPartTime(date.getDate());
           $('#bed-availability_date').val(str);
           $('.field-bed-availability_date').css('display','none');
       }else
       {
           $('.bed-availability_date').css('disable','false');
           $('.field-bed-availability_date').css('display','block');
       }  
   });
   

$('.del-image').on('click',function()
 {
    var imageName = $(this).attr('id');
   var stateId = $('#current-state-id').val();
     $.ajax({
         type:'GET',
         url:"/mub-admin/real-estate/property/image-delete?name="+imageName+'&state='+stateId,
         success: function(result)
         {
          alert(result);
          location.reload();        
         }
        }); 
});
