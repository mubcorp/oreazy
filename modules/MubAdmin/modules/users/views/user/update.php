<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $mubUser app\models\MubUser */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'User',
]) . $mubUser->first_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mub Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $mubUser->first_name, 'url' => ['view', 'id' => $mubUser->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mub-user-update">
<div class="col-md-10 col-md-offset-1">
    <p><?= Html::encode($this->title) ?></p>
</div>
    <?= $this->render('_form', [
        'mubUser' => $mubUser,
        'mubUserContacts' => $mubUserContacts,
        'allStates' => $allStates,
        'mubUserStates' => $mubUserStates,
        'activeStates' => $activeStates
    ]) ?>

</div>