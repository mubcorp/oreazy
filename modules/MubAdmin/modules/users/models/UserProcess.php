<?php 

namespace app\modules\MubAdmin\modules\users\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\models\User;
use app\models\MubUser;
use app\models\MubUserContact;
use app\models\UserStates;
use yii\helpers\Url;

class UserProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $mubUser = new MubUser(); 
        $mubUserContacts = new MubUserContact();
        $mubUserStates = new UserStates(); 
        $mubUser->scenario = 'create_mub_user';
        $mubUserContacts->scenario = 'create_mub_user';
        $mubUserStates->scenario = 'create_mub_user';

        $this->models = [
            'mubUser' => $mubUser,
            'mubUserContacts' => $mubUserContacts,
            'mubUserStates' => $mubUserStates
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $state = new \app\models\State();
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0','active' => '1'];
        $activeStates = $state->getAll('state_name',$where);
        return ['allStates' => $allStates,'activeStates' => $activeStates];
    }

    public function saveUserStates($userStates,$mubUserId)
    {
        $recordSet = [];
        $attribs = ['mub_user_id','state_id','created_at'];
        foreach ($userStates->state_id as $state) 
        {
            $recordSet[] = [$mubUserId,$state,date('Y-m-d h:m:s',time())];
        }
        $uStates = new UserStates();
        $uStates::deleteAll(['mub_user_id' => $mubUserId]);
        $recordCount = count($userStates->state_id);
        $insertedCount = \Yii::$app->db->createCommand()->batchInsert($uStates->tableName(), $attribs, $recordSet)->execute();

        if($recordCount == $insertedCount)
        {
            return true;
        }
        p($uStates->getErrors());
    }

    public function getUserStates($userStates)
    {
        $uStates = [];
        foreach ($userStates as $state) {
            $uStates[] = $state->state_id; 
        }
        return $uStates;
    }

    public function getRelatedModels($model)
    {
        $mubUser = $model;
        $mubUserContacts = $model->mubUserContacts;
        if(\Yii::$app->controller->action->id == 'update' || \Yii::$app->controller->action->id == 'view')
        {
            $userStates = $this->getUserStates($model->mubUserState);
            $mubUserStates = (!empty($userStates)) ? $model->mubUserState[0] : new UserStates();
            $mubUserStates->state_id = $userStates;
            $mubUser->scenario = 'update_mub_user';
            $mubUserContacts->scenario = 'update_mub_user';
            $mubUserStates->scenario = 'update_mub_user';
            $this->relatedModels = [
                'mubUser' => $mubUser,
                'mubUserContacts' => $mubUserContacts,
                'mubUserStates' => $mubUserStates
            ];   
        }
        elseif(\Yii::$app->controller->action->id == 'delete')
        {
            $mubUserStates = $model->mubUserState;
            $this->relatedModels = [
                'mubUser' => $mubUser,
                'mubUserContacts' => $mubUserContacts,
                'mubUserStates' => $mubUserStates
            ];  
        }
        return $this->relatedModels;
    }

    private function saveUserData($mubUser)
    {
        if($mubUser->id)
        {
            $userModel = new User();
            $user = $userModel::findOne($mubUser->user_id);
        }
        else
        {
            $user = new User();
        }
        $user->first_name = $mubUser->first_name;
        $user->last_name = $mubUser->last_name;
        $user->username = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $mubUser->username));
        $user->password = $mubUser->password;
        $user->dob = ($mubUser->dob) ? $mubUser->dob : '1970-01-01 12:00:00';
        $user->gender = ($mubUser->gender) ? $mubUser->gender : 'Male';
        $user->status = ($mubUser->status) ? $mubUser->status : 'Inactive';
        $user->setPassword($mubUser->password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        if($user->save())
        {
            if(\Yii::$app->controller->action->id == 'create')
            {
                $auth = \Yii::$app->authManager;
                $subadmin = $auth->createRole('subadmin');
                $auth->assign($subadmin, $user->id);
            }
            $mubUser->user_id = $user->id;
            $mubUser->domain = ($mubUser->domain) ? $mubUser->domain : 'www.yourwebsite.com';
            $mubUser->role = 'subadmin';
            $mubUser->status = ($mubUser->status) ? $mubUser->status : 'Inactive';
            $mubUser->organization = ($mubUser->organization) ? $mubUser->organization : "Your organization";
            $mubUser->username = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $mubUser->username));
            $mubUserContact = new \app\models\MubUserContact();
            if($mubUser->save())
            {
                return $mubUser->id;
            }
               throw new \yii\web\HttpException(500, 'User saved but not MubUser'); 
        }
        p($user->getErrors());
    }


    private function saveMubUserContact($mubUserContacts,$mubUserId)
    {
        $mubUserContacts->mub_user_id = $mubUserId;
        $mubUserContacts->city = ($mubUserContacts->city) ? $mubUserContacts->city : 125;
        $mubUserContacts->email = ($mubUserContacts->email) ? $mubUserContacts->email : 'youremail@domain.com';
        $mubUserContacts->pin_code = "110089";
        $mubUserContacts->landline = '023456789';
        $mubUserContacts->mobile = ($mubUserContacts->mobile) ? $mubUserContacts->mobile : '0987654321';
        $mubUserContacts->address = ($mubUserContacts->address) ? $mubUserContacts->address : 'Your complete address';
        return ($mubUserContacts->save()) ? $mubUserContacts->id : p($mubUserContacts->getErrors());
    } 

    public function saveData($data = [])
    {
if (isset($data['mubUser'])&&
    isset($data['mubUserContacts'])&&
    isset($data['mubUserStates']))
    {
    try {
        $mubUserId = $this->saveUserData($data['mubUser']);
        if ($mubUserId)
        {
            $mubUserContact = $this->saveMubUserContact($data['mubUserContacts'],$mubUserId);
            if($mubUserContact)
            {
                $userStates = $this->saveUserStates($data['mubUserStates'],$mubUserId);
                if($userStates)
                {
                    if(\Yii::$app->controller->action->id == 'create')
                    {
                    $mubUserModel = new \app\models\MubUser();
                    $mubUser = $mubUserModel::findOne($mubUserId);
                    $mubUserContact = $mubUser->mubUserContacts;
                    \Yii::$app->mailer->compose('welcome',['mubUser' => $mubUser,'mubUserContact' => $mubUserContact])
                    ->setFrom('info@Furniture Minds.com')
                    ->setTo($data['mubUserContacts']->email)
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->setSubject('Your Profile Created')     
                    ->send();
                    }
                    return $mubUserId;
                }  
            }
                    else
                    {
                        throw new \yii\web\HttpException(500, 'User Contact Data not saved');
                    }
                } 
                else
                {
                    throw new \yii\web\HttpException(500, 'User data not saved');
                } 
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}