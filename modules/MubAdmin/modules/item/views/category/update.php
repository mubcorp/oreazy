<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $category app\modules\MubAdmin\modules\item\categorys\Category */

$this->title = 'Update Category: ' . $category->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-update">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="card">
				<div class="header">
					<p><?= Html::encode($this->title) ?></p>

				    <?= $this->render('_form', [
				        'category' => $category,
				    ]) ?>
				</div>
            </div>
        </div>
    </div>
</div>
