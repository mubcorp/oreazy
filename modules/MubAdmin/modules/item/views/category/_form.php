<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$action = \Yii::$app->controller->action->id;
$validateUrl = '/validate/catagory-validate?action='.$action;
if($action == 'update')
{
    $id = \Yii::$app->request->getQueryParam('id');
    $validateUrl = $validateUrl.'&id='.$id;
}

/* @var $this yii\web\View */
/* @var $category app\modules\MubAdmin\modules\item\categorys\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

<?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validationUrl' => [$validateUrl],'options' => ['method' => 'POST','data-pjax' => true]]); ?>

   <div class="row"> 
        <div class="col-md-6"><?= $form->field($category, 'name')->textInput(['maxlength' => true,'class'=>'border-input form-control']) ?></div>

        <div class="col-md-6">
        <?= $form->field($category, 'type')->dropDownList(['shiping' => 'Shiping','planning' => 'Planning','item' => 'Item'], ['prompt' => 'Select Category Type', 'id'=>'show_value','class'=>'border-input form-control']) ?>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-12"><?= $form->field($category, 'description')->textarea(['rows' => 6,'class'=>'border-input form-control']) ?></div>
    </div>
    <div class="row" id="product-image"> 
        <div class="col-md-12">
            <?= $form->field($category, 'image')->fileInput(['class' => 'border-input form-control','id' => 'product-image-selector']); ?></div>
        <!-- <div class="col-md-5"><?php //$form->field($category, 'priority')->textInput(['maxlength' => true]) ?></div> -->
    </div>
    <?php if((\Yii::$app->controller->action->id == 'update') && ($category->type == "item") &&($category->image !== '')){?>
    <div class="row text-center">
        <div class="col-md-6">
            <img src="/<?= $category->image;?>" class="img-responsive">
        </div>
    </div>
    <?php }?>
    <div class="row"> 
        <div class="col-md-12 text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end();?>
</div>