<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$action = \Yii::$app->controller->action->id;
$validateUrl = '/validate/manufacture-validate?action='.$action;
if($action == 'update')
{
	$id = \Yii::$app->request->getQueryParam('id');
	$validateUrl = $validateUrl.'&id='.$id;
}
/* @var $this yii\web\View */
/* @var $manufacture app\modules\MubAdmin\modules\item\manufactures\Manufacture */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validationUrl' => [$validateUrl],'options' => ['method' => 'GET','data-pjax' => true]]); ?>
<div class="row">
	<div class="col-md-12">
    	<?= $form->field($manufacture, 'name')->textInput(['maxlength' => true,'class' => 'border-input form-control']) ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
    	<?= $form->field($manufacture, 'image')->fileInput(['class' => 'border-input form-control']); ?>
	</div>
</div>
<?php if(\Yii::$app->controller->action->id == 'update' && $manufacture->image !=''){?>
<div class="row text-center">
	<div class="col-md-6">
    	<img src="/<?= $manufacture->image;?>" class="img-responsive">
	</div>
</div>
	<?php }?>
<div class="row">
    <div class="col-md-12 text-center">
    	<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
