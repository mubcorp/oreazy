<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $manufacture app\modules\MubAdmin\modules\item\manufactures\Manufacture */

$this->title = 'Update Manufacturer: '. $manufacture->name;
$this->params['breadcrumbs'][] = ['label' => 'Manufactures', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $manufacture->name, 'url' => ['view', 'id' => $manufacture->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="manufacture-update">

    <div class="row">
		<div class="col-lg-6 col-md-5 col-md-offset-2">
			<div class="card">
			    <div class="header"><?= Html::encode($this->title) ?></div>
			    <div class="content">
			    	<?= $this->render('_form', [
			        'manufacture' => $manufacture,
			    	]) ?>
			    </div>
		    </div>
		</div>
	</div>
</div>