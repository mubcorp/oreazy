<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $manufacture app\modules\MubAdmin\modules\item\manufactures\Manufacture */

$this->title = 'Create Manufacturer';
$this->params['breadcrumbs'][] = ['label' => 'Manufactures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-6 col-md-5 col-md-offset-3">
		<div class="card">
	    	<div class="header"><?= Html::encode($this->title) ?></div>
	    	<div class="content">
			    <?= 
			    	$this->render('_form', [
			        'manufacture' => $manufacture,
			    	]) 
			    ?>
			</div>
		</div>
	</div>
</div>