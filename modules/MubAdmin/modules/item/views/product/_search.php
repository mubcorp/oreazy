<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\item\products\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($product, 'id') ?>

    <?= $form->field($product, 'mub_user_id') ?>

    <?= $form->field($product, 'model_number') ?>

    <?= $form->field($product, 'item_package_quantity') ?>

    <?= $form->field($product, 'size') ?>

    <?php // echo $form->field($product, 'size_map') ?>

    <?php // echo $form->field($product, 'color') ?>

    <?php // echo $form->field($product, 'color_map') ?>

    <?php // echo $form->field($product, 'enclosure_material') ?>

    <?php // echo $form->field($product, 'manufacturer') ?>

    <?php // echo $form->field($product, 'title') ?>

    <?php // echo $form->field($product, 'manufacturer_part_number') ?>

    <?php // echo $form->field($product, 'brand') ?>

    <?php // echo $form->field($product, 'category') ?>

    <?php // echo $form->field($product, 'sub_category') ?>

    <?php // echo $form->field($product, 'seller_sku') ?>

    <?php // echo $form->field($product, 'HSN_code') ?>

    <?php // echo $form->field($product, 'price') ?>

    <?php // echo $form->field($product, 'sale_price') ?>

    <?php // echo $form->field($product, 'warranty_des') ?>

    <?php // echo $form->field($product, 'country') ?>

    <?php // echo $form->field($product, 'release_date') ?>

    <?php // echo $form->field($product, 'sale_start_date') ?>

    <?php // echo $form->field($product, 'sale_end_date') ?>

    <?php // echo $form->field($product, 'restock_date') ?>

    <?php // echo $form->field($product, 'offer_release_date') ?>

    <?php // echo $form->field($product, 'condition_notes') ?>

    <?php // echo $form->field($product, 'leagle_notes') ?>

    <?php // echo $form->field($product, 'quantity') ?>

    <?php // echo $form->field($product, 'max_order_quantity') ?>

    <?php // echo $form->field($product, 'product_text_code') ?>

    <?php // echo $form->field($product, 'condition') ?>

    <?php // echo $form->field($product, 'handling_time') ?>

    <?php // echo $form->field($product, 'gift_wrap') ?>

    <?php // echo $form->field($product, 'status') ?>

    <?php // echo $form->field($product, 'created_at') ?>

    <?php // echo $form->field($product, 'updated_at') ?>

    <?php // echo $form->field($product, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
