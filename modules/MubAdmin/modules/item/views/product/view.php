<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\Product */

$this->title = $product->title;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="header">
                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a('Update', ['update', 'id' => $product->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $product->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $product,
                    'attributes' => [
                        'id',
                        'mub_user_id',
                        'model_number',
                        'item_package_quantity',
                        'size',
                        'size_map',
                        'color',
                        'color_map',
                        'enclosure_material',
                        'manufacturer',
                        'title',
                        'manufacturer_part_number',
                        'brand',
                        'category',
                        'sub_category',
                        'seller_sku',
                        'HSN_code',
                        'price',
                        'sale_price',
                        'warranty_des',
                        'country',
                        'release_date',
                        'sale_start_date',
                        'sale_end_date',
                        'restock_date',
                        'offer_release_date',
                        'condition_notes',
                        'leagle_notes',
                        'quantity',
                        'max_order_quantity',
                        'product_text_code',
                        'condition',
                        'handling_time',
                        'gift_wrap',
                        'status',
                        'created_at',
                        'updated_at',
                        'del_status',
                    ],
                ]) ?>
             <br/>
            </div>
            </div>
        </div>
    </div>
</div>
