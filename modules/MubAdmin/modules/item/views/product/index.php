<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\item\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                        <h1><?= Html::encode($this->title) ?></h1>
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                        </div>
                        <div class="col-md-6 text-right">
                        <p>
                            <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
                        </p>
                        </div>
                    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'mub_user_id',
            'model_number',
            // 'item_package_quantity',
            // 'size',
            //'size_map',
            //'color',
            //'color_map',
            //'enclosure_material',
            //'manufacturer',
            'title',
            //'manufacturer_part_number',
            //'brand',
            'category',
            'sub_category',
            //'seller_sku',
            //'HSN_code',
            'price',
            //'sale_price',
            //'warranty_des',
            //'country',
            //'release_date',
            //'sale_start_date',
            //'sale_end_date',
            //'restock_date',
            //'offer_release_date',
            //'condition_notes',
            //'leagle_notes',
            //'quantity',
            //'max_order_quantity',
            //'product_text_code',
            //'condition',
            //'handling_time',
            //'gift_wrap',
            'status',
            //'created_at',
            //'updated_at',
            //'del_status',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <br/>
               </div>
            </div>
        </div>
    </div>
</div>

