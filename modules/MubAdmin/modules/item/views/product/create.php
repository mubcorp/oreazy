<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\item\products\Product */

$this->title = 'Create Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
    		<div class="card">
    			<div class="header">

				    <p><?= Html::encode($this->title) ?></p>

				    <?= $this->render('_form', [
				        'product' => $product,
				        'allcategory' => $allcategory,
				        'allsubcategory' => $allsubcategory,
				    ]) ?>

				</div>
        </div>
    </div>
</div>