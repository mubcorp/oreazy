<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\item\products\Product */

$this->title = 'Update Product: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $product->title, 'url' => ['view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'product' => $product,
        'allcategory' => $allcategory,
	    'allsubcategory' => $allsubcategory,
    ]) ?>

</div>
