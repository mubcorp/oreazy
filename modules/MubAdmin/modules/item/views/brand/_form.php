<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$action = \Yii::$app->controller->action->id;
$validateUrl = '/validate/brand-validate?action='.$action;
if($action == 'update')
{
    $id = \Yii::$app->request->getQueryParam('id');
    $validateUrl = $validateUrl.'&id='.$id;
}
/* @var $this yii\web\View */
/* @var $brand app\modules\MubAdmin\modules\item\brands\Brand */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-form">

<?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validationUrl' => [$validateUrl],'options' => ['method' => 'POST','data-pjax' => true]]); ?>

    <?= $form->field($brand, 'name')->textInput(['maxlength' => true,'class' => 'border-input form-control']) ?>

    <?php echo $form->field($brand, 'manufacture_id')->dropDownList($allmanufacture,['class' => 'border-input form-control'])->label('Select Manufacturer');?>

    <?= $form->field($brand, 'logo')->fileInput(['class' => 'border-input form-control']); ?>

    <?php if(\Yii::$app->controller->action->id == 'update'){?>
    <div class="row text-center">
        <div class="col-md-6">
            <img src="/<?= $brand->logo;?>" class="img-responsive">
        </div>
    </div>
    <?php }?>

    <div class="row">
	    <div class="col-md-12 text-center">
	        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
