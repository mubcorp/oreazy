<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\ImageUploader;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\Brand */

$this->title = $brand->name;
$this->params['breadcrumbs'][] = ['label' => 'Brands', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-view">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="header">
                    <p><?= Html::encode($this->title) ?></p>

                    <p>
                        <?= Html::a('Update', ['update', 'id' => $brand->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $brand->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>

                    <?= DetailView::widget([
                        'model' => $brand,
                        'attributes' => [
                            // 'manufacture_id',
                            'name',
                            [
                                'format' => 'image',
                                'attribute' => 'logo',
                                'contentOptions'=>['class' =>'logo_image'],
                                'value' => function($dataProvider)
                                {
                                    if($dataProvider->logo != '')
                                    {

                                        $image = ImageUploader::resizeRender("/".$dataProvider->logo,100,50);
                                    }
                                    else
                                    {
                                        $image = ImageUploader::resizeRender("/uploads/not-found.png",100,50);
                                    }
                                    return $image; 
                                }
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
