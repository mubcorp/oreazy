<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\Subcat */

$this->title = 'Create Subcat';
$this->params['breadcrumbs'][] = ['label' => 'Subcats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcat-create">
	<div class="row">
    	<div class="col-md-6 col-md-offset-3">
    		<div class="card">
    		<div class="header">
		    <p><?= Html::encode($this->title) ?></p>
		    <?= $this->render('_form', [
		        'subcat' => $subcat,
		        'allcategory' => $allcategory,
		    ]) ?>
		    </div>
		    </div>
		</div>
	</div>

</div>