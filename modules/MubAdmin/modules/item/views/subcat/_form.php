<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$action = \Yii::$app->controller->action->id;
$validateUrl = '/validate/subcatagory-validate?action='.$action;
if($action == 'update')
{
    $id = \Yii::$app->request->getQueryParam('id');
    $validateUrl = $validateUrl.'&id='.$id;
}

/* @var $this yii\web\View */
/* @var $subcat app\modules\MubAdmin\modules\item\subcats\Subcat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subcat-form">
    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validationUrl' => [$validateUrl],'options' => ['method' => 'POST','data-pjax' => true]]); ?>
        <div class="row"> 
            <div class="col-md-6">
                <?= $form->field($subcat, 'name')->textInput(['maxlength' => true, 'class' => 'border-input form-control']) ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($subcat, 'item_category_id')->dropDownList($allcategory,['class' => 'border-input form-control'])->label('Select Category'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php $form->field($subcat, 'priority')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($subcat, 'description')->textarea(['rows' => 6,'class' => 'border-input form-control']) ?>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-12">
                <?= $form->field($subcat, 'image')->fileInput(['class' => 'border-input form-control']); ?>
            </div>
        </div>
        <?php if(\Yii::$app->controller->action->id == 'update'){?>
        <div class="row text-center">
            <div class="col-md-6">
                <img src="/<?= $subcat->image;?>" class="img-responsive">
            </div>
        </div>
        <?php }?>
        <div class="row">
            <div class="col-md-12 text-center">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>