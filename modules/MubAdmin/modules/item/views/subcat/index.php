<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helpers\ImageUploader;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\item\models\SubcatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subcats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcat-index">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="header">
                    <div class="row">
                    <div class="col-md-6">
                    <?= Html::encode($this->title) ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    </div>
                    <div class="col-md-6">
                    <p style="text-align: right;">
                        <?= Html::a('Create Sub Category', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                    </div>
                    </div>
                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'name',
                        //'priority',
                        //'description:ntext',
                        [
                            'format' => 'image',
                            'attribute' => 'image',
                            'contentOptions'=>['class' =>'logo_image'],
                            'value' => function($dataProvider){
                                if($dataProvider->image != '')
                                    {

                                        $image = ImageUploader::resizeRender("/".$dataProvider->image,100,50);
                                    }
                                    else
                                    {
                                        $image = ImageUploader::resizeRender("/uploads/not-found.png",100,50);
                                    }
                                    return $image; 
                            }
                        ],
                        //'created_at',
                        //'updated_at',
                        //'del_status',

                        ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

