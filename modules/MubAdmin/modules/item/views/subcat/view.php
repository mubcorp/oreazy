<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\ImageUploader;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\Subcat */

$this->title = $subcat->name;
$this->params['breadcrumbs'][] = ['label' => 'Subcats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcat-view">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="header">

                    <p><?= Html::encode($this->title) ?></p>

                    <p>
                        <?= Html::a('Update', ['update', 'id' => $subcat->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $subcat->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>

                    <?= DetailView::widget([
                        'model' => $subcat,
                        'attributes' => [
                            'name',
                            'description:ntext',
                            [
                            'format' => 'image',
                            'attribute' => 'image',
                            'contentOptions'=>['class' =>'logo_image'],
                            'value' => function($dataProvider){
                                if($dataProvider->image != '')
                                    {

                                        $image = ImageUploader::resizeRender("/".$dataProvider->image,100,50);
                                    }
                                    else
                                    {
                                        $image = ImageUploader::resizeRender("/uploads/not-found.png",100,50);
                                    }
                                    return $image;                           
                            }
                        ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
