<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $subcat app\modules\MubAdmin\modules\item\subcats\Subcat */

$this->title = 'Update Subcat: ' . $subcat->name;
$this->params['breadcrumbs'][] = ['label' => 'Subcats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $subcat->name, 'url' => ['view', 'id' => $subcat->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subcat-update">
	<div class="row">
	    <div class="col-md-6 col-md-offset-3">
		<div class="card">
			<div class="header">
		    	<p><?= Html::encode($this->title) ?></p>

		    	<?= $this->render('_form', [
			        'subcat' => $subcat,
			        'allcategory' => $allcategory,
			    ]) ?>
		    </div>
	    </div>
		</div>
	</div>
</div>
