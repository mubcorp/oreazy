<?php

namespace app\modules\MubAdmin\modules\item\controllers;

use Yii;
use app\modules\MubAdmin\modules\item\models\Manufacture;
use app\modules\MubAdmin\modules\item\models\ManufactureSearch;
use app\modules\MubAdmin\modules\item\models\ManufactureProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ManufactureController implements the CRUD actions for Manufacture model.
 */
class ManufactureController extends MubController
{
    public function getPrimaryModel()
   {
        return new Manufacture();
   }

   public function getProcessModel()
   {
        return new ManufactureProcess();
   }

   public function getSearchModel()
   {
        return new ManufactureSearch();
   }
}