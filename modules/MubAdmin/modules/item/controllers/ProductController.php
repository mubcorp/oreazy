<?php

namespace app\modules\MubAdmin\modules\item\controllers;

use Yii;
use app\components\MubController;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use app\modules\MubAdmin\modules\item\models\Product;
use app\modules\MubAdmin\modules\item\models\ProductSearch;
use app\modules\MubAdmin\modules\item\models\ProductProcess;
use app\modules\MubAdmin\modules\item\models\ProductImages;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Default controller for the `Product` module
 */
class ProductController extends MubController
{
    public function getPrimaryModel()
   {
        return new Product();
   }

   public function getProcessModel()
   {
        return new ProductProcess();
   }

   public function getSearchModel()
   {
        return new ProductSearch();
   }

   public function actionProductImage($userId)
   {
    if (\Yii::$app->request->isAjax) {
      $model = new ProductImages();

      $imageFile = UploadedFile::getInstance($model, 'thumbnail_url');

      $directory = \Yii::getAlias('@app/images/product') . DIRECTORY_SEPARATOR.$userId;
      if (!is_dir($directory)) {
          FileHelper::createDirectory($directory);
      }

      if ($imageFile) {
          $uid = uniqid(time(), true);
          $fileName = $uid . '.' . $imageFile->extension;
          $filePath = $directory . $fileName;
          if ($imageFile->saveAs($filePath)) {
              $path = '/images/product/'. $userId.DIRECTORY_SEPARATOR.$fileName;
              $model->mub_user_id = $userId;
              $model->item_id = NULL;
              $model->full_path = $path;
              $model->thumbnail_url = $path;
              if(!$model->save())
              {
                p($model->getErrors());
              }
              
              $successPath = '/images/done.png';
              return Json::encode([
                  'files' => [
                      [
                          'name' => $fileName,
                          'size' => $imageFile->size,
                          'url' => $successPath,
                          'thumbnailUrl' => $successPath,
                          'deleteUrl' => 'image-delete?name=' . $fileName,
                          'deleteType' => 'POST',
                      ],
                  ],
              ]);
          }
      }
      return '';
    }else
    {
      return 'go away';
    }
  }

  public function actionImageDelete($name)
  {
     if (\Yii::$app->request->isAjax) {
      $stateId = \Yii::$app->request->getQueryParam('state');
      $directory = \Yii::getAlias('@app/images/product') . DIRECTORY_SEPARATOR;
      if (is_file($directory.$name)) 
      {
          if(unlink($directory . $name))
          {
            $productImages = new \app\modules\MubAdmin\modules\item\models\ProductImages();
            $success = $productImages::deleteAll(['title' => $name]);
            if($success)
            {
              return 'Image Deleted Successfully'; 
            }
          }else
          {
            return 'There was a problem deleting the Image';
          }
      }
      else
      {
        $productImages = new \app\modules\MubAdmin\modules\item\models\ProductImages();
        $success = $productImages::deleteAll(['title' => $name]);
        if($success)
        {
          return 'Image Deleted from Records'; 
        }
      }
      
  }else
  {
    return 'Go Awayyyy!';
  }
}

}
