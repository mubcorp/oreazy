<?php 
namespace app\modules\MubAdmin\modules\item\models;

use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class ProductProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $product = new Product();
        $this->models = [
            'product' => $product,
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $category = new Category();
        $subcategory = new Subcat();
        $brand = new Brand();
        $where = ['del_status' => '0','status' => 'active'];
        $allcategory = $category->getAll('name',$where);
        $allsubcategory = $subcategory->getAll('name',$where);
        $allbrand = $brand->getAll('name',$where);
        return [
            'allcategory' => $allcategory,
            'allsubcategory' => $allsubcategory,
            'allbrand' => $allbrand,
        ];
    }

    public function getRelatedModels($model)
    {
        $product = $model;
        $this->relatedModels = [
            'product' => $product,
        ];
        return $this->relatedModels;
    }

    public function saveProduct($product)
    {
        $userId = \app\models\User::getMubUserId();
        $product->mub_user_id =  $userId;
        return ($product->save()) ? $product->id : p($product->getErrors());
    }

    
   public function saveData($data)
    {
        if(isset($data['product']))
        {
        try {
            $catId = $this->saveProduct($data['product']);
            return ($catId) ? $catId : false;  
            }
            catch (\Exception $e)
            {
                throw $e;
            }
        }
        throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
