<?php

namespace app\modules\MubAdmin\modules\item\models;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use Yii;

/**
 * This is the model class for table "manufacture".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $logo
 * @property string $barcode
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property Brand[] $brands
 */
class Manufacture extends \app\components\MOdel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
            [['image'],'checkImageExists','on' => 'update'],
            [['status', 'del_status'], 'string'],
            ['name', 'unique', 'targetClass' => '\app\modules\MubAdmin\modules\item\models\Manufacture', 'message' => 'Manufacturer by this name already exists.'],
            [['created_at', 'updated_at'], 'safe'],
            [['id', 'mub_user_id'], 'integer'],
            [['name', 'slug','barcode'], 'string', 'max' => 255],
            [['name', 'status', 'del_status'], 'unique', 'targetAttribute' => ['name', 'status', 'del_status']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['mub_user_id','image','status','name','created_at','slug','del_status'];
        $scenarios['update'] = ['mub_user_id','image','name','slug','updated_at'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'image' => 'Logo',
            'barcode' => 'Barcode',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrands()
    {
        return $this->hasMany(Brand::className(), ['manufacture_id' => 'id']);
    }
    
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
