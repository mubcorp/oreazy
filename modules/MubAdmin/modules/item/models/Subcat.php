<?php

namespace app\modules\MubAdmin\modules\item\models;

use Yii;

/**
 * This is the model class for table "item_sub_category".
 *
 * @property int $id
 * @property int $item_category_id
 * @property string $name
 * @property string $slug
 * @property string $priority
 * @property string $description
 * @property string $image
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property ItemCategory $itemCategory
 */
class Subcat extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_sub_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_category_id', 'name'], 'required'],
            [['item_category_id','id', 'mub_user_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
            [['image'],'checkImageExists','on' => 'update'],
            ['name', 'unique', 'targetClass' => '\app\modules\MubAdmin\modules\item\models\Subcat', 'message' => 'Subcat by this name already exists.'],
            [['description', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug', 'priority', 'image'], 'string', 'max' => 255],
            [['name', 'status', 'del_status'], 'unique', 'targetAttribute' => ['name', 'status', 'del_status']],
            // [['item_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemCategory::className(), 'targetAttribute' => ['item_category_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['mub_user_id','image','status','name','created_at','slug','item_category_id'];
        $scenarios['update'] = ['mub_user_id','image','name','slug','updated_at'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_category_id' => 'Item Category ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'priority' => 'Priority',
            'description' => 'Description',
            'image' => 'Image',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategory()
    {
        return $this->hasOne(ItemCategory::className(), ['id' => 'item_category_id']);
    }

    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
