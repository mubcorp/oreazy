<?php

namespace app\modules\MubAdmin\modules\item\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\item\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\modules\MubAdmin\modules\item\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'manufacturer', 'manufacturer_part_number', 'brand', 'price', 'sale_price', 'quantity', 'max_order_quantity'], 'integer'],
            [['model_number', 'item_package_quantity', 'size', 'size_map', 'color', 'color_map', 'enclosure_material', 'title', 'category', 'sub_category', 'seller_sku', 'HSN_code', 'warranty_des', 'country', 'release_date', 'sale_start_date', 'sale_end_date', 'restock_date', 'offer_release_date', 'condition_notes', 'leagle_notes', 'product_text_code', 'condition', 'handling_time', 'gift_wrap', 'status', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'manufacturer' => $this->manufacturer,
            'manufacturer_part_number' => $this->manufacturer_part_number,
            'brand' => $this->brand,
            'price' => $this->price,
            'sale_price' => $this->sale_price,
            'quantity' => $this->quantity,
            'max_order_quantity' => $this->max_order_quantity,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'model_number', $this->model_number])
            ->andFilterWhere(['like', 'item_package_quantity', $this->item_package_quantity])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'size_map', $this->size_map])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'color_map', $this->color_map])
            ->andFilterWhere(['like', 'enclosure_material', $this->enclosure_material])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'sub_category', $this->sub_category])
            ->andFilterWhere(['like', 'seller_sku', $this->seller_sku])
            ->andFilterWhere(['like', 'HSN_code', $this->HSN_code])
            ->andFilterWhere(['like', 'warranty_des', $this->warranty_des])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'release_date', $this->release_date])
            ->andFilterWhere(['like', 'sale_start_date', $this->sale_start_date])
            ->andFilterWhere(['like', 'sale_end_date', $this->sale_end_date])
            ->andFilterWhere(['like', 'restock_date', $this->restock_date])
            ->andFilterWhere(['like', 'offer_release_date', $this->offer_release_date])
            ->andFilterWhere(['like', 'condition_notes', $this->condition_notes])
            ->andFilterWhere(['like', 'leagle_notes', $this->leagle_notes])
            ->andFilterWhere(['like', 'product_text_code', $this->product_text_code])
            ->andFilterWhere(['like', 'condition', $this->condition])
            ->andFilterWhere(['like', 'handling_time', $this->handling_time])
            ->andFilterWhere(['like', 'gift_wrap', $this->gift_wrap])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
