<?php

namespace app\modules\MubAdmin\modules\item\models;

use Yii;

/**
 * This is the model class for table "item_category".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $slug
 * @property string $priority
 * @property string $description
 * @property string $image
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property ItemSubCategory[] $itemSubCategories
 */
class Category extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','type'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
            [['image'],'checkImageExists','on' => 'update'],
            ['name', 'unique', 'targetClass' => '\app\modules\MubAdmin\modules\item\models\Category', 'message' => 'Category by this name already exists.'],
            [['type', 'description', 'status', 'del_status','shiping_value','plannig_value'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug', 'priority', 'image'], 'string', 'max' => 255],
            [['name', 'status', 'del_status'], 'unique', 'targetAttribute' => ['name', 'status', 'del_status']],
        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['image','status','type','description','name','created_at','slug'];
        $scenarios['update'] = ['image','description','type','name','slug','updated_at'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'shiping_value' => 'Shiping Value',
            'plannig_value' => 'Plannig Value',
            'slug' => 'Slug',
            'priority' => 'Priority',
            'description' => 'Description',
            'image' => 'Image',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemSubCategories()
    {
        return $this->hasMany(ItemSubCategory::className(), ['item_category_id' => 'id']);
    }

    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
