<?php

namespace app\modules\MubAdmin\modules\item\models;

use Yii;

/**
 * This is the model class for table "brand".
 *
 * @property int $id
 * @property int $manufacture_id
 * @property string $name
 * @property string $slug
 * @property string $logo
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property Manufacture $manufacture
 */
class Brand extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacture_id', 'name'], 'required'],
            [['manufacture_id'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['logo'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
            ['name', 'unique', 'targetClass' => '\app\modules\MubAdmin\modules\item\models\Brand', 'message' => 'Brand by this name already exists.'],
            [['logo'],'checkImageExists','on' => 'update'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['name', 'status', 'del_status'], 'unique', 'targetAttribute' => ['name', 'status', 'del_status']],
            [['manufacture_id'], 'exist', 'skipOnError' => true, 'targetClass' => Manufacture::className(), 'targetAttribute' => ['manufacture_id' => 'id']],
        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['image','status','name','created_at','logo','slug'];
        $scenarios['update'] = ['image','name','slug','logo','updated_at'];
        return $scenarios;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacture_id' => 'Manufacture ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'logo' => 'Logo',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacture()
    {
        return $this->hasOne(Manufacture::className(), ['id' => 'manufacture_id']);
    }

    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
