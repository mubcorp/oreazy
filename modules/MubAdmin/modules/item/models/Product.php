<?php

namespace app\modules\MubAdmin\modules\item\models;
use app\models\MubUser;
use Yii;

/**
 * This is the model class for table "item".
 *
 * @property int $id
 * @property int $mub_user_id
 * @property string $model_number
 * @property string $item_package_quantity
 * @property string $size
 * @property string $size_map
 * @property string $color
 * @property string $color_map
 * @property string $enclosure_material
 * @property int $manufacturer
 * @property string $title
 * @property int $manufacturer_part_number
 * @property int $brand
 * @property string $category
 * @property string $sub_category
 * @property string $seller_sku
 * @property string $HSN_code
 * @property int $price
 * @property int $sale_price
 * @property string $warranty_des
 * @property string $country
 * @property string $release_date
 * @property string $sale_start_date
 * @property string $sale_end_date
 * @property string $restock_date
 * @property string $offer_release_date
 * @property string $condition_notes
 * @property string $leagle_notes
 * @property int $quantity
 * @property int $max_order_quantity
 * @property string $product_text_code
 * @property string $condition
 * @property string $handling_time
 * @property string $gift_wrap
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property MubUser $mubUser
 * @property ItemImages[] $itemImages
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'manufacturer', 'manufacturer_part_number', 'brand', 'price', 'sale_price', 'quantity', 'max_order_quantity'], 'integer'],
            [['item_package_quantity', 'gift_wrap', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['model_number', 'size', 'size_map', 'color', 'color_map', 'enclosure_material', 'title', 'category', 'sub_category', 'seller_sku', 'HSN_code', 'warranty_des', 'country', 'release_date', 'sale_start_date', 'sale_end_date', 'restock_date', 'offer_release_date', 'condition_notes', 'leagle_notes', 'product_text_code', 'condition', 'handling_time'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'model_number' => 'Model Number',
            'item_package_quantity' => 'Item Package Quantity',
            'size' => 'Size',
            'size_map' => 'Size Map',
            'color' => 'Color',
            'color_map' => 'Color Map',
            'enclosure_material' => 'Enclosure Material',
            'manufacturer' => 'Manufacturer',
            'title' => 'Title',
            'manufacturer_part_number' => 'Manufacturer Part Number',
            'brand' => 'Brand',
            'category' => 'Category',
            'sub_category' => 'Sub Category',
            'seller_sku' => 'Seller Sku',
            'HSN_code' => 'Hsn Code',
            'price' => 'Price',
            'sale_price' => 'Sale Price',
            'warranty_des' => 'Warranty Des',
            'country' => 'Country',
            'release_date' => 'Release Date',
            'sale_start_date' => 'Sale Start Date',
            'sale_end_date' => 'Sale End Date',
            'restock_date' => 'Restock Date',
            'offer_release_date' => 'Offer Release Date',
            'condition_notes' => 'Condition Notes',
            'leagle_notes' => 'Leagle Notes',
            'quantity' => 'Quantity',
            'max_order_quantity' => 'Max Order Quantity',
            'product_text_code' => 'Product Text Code',
            'condition' => 'Condition',
            'handling_time' => 'Handling Time',
            'gift_wrap' => 'Gift Wrap',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemImages()
    {
        return $this->hasMany(ItemImages::className(), ['item_id' => 'id']);
    }
}
