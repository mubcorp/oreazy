<?php

namespace app\modules\MubAdmin\modules\item\models;

use Yii;

/**
 * This is the model class for table "item_images".
 *
 * @property int $id
 * @property int $item_id
 * @property string $thumbnail_url
 * @property string $thumbnail_path
 * @property string $full_path
 * @property string $visible
 * @property string $keyword
 * @property int $width
 * @property int $height
 * @property int $display_width
 * @property int $display_hieght
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status 0-Active,1-Deleted DEFAULT 0
 *
 * @property Item $item
 */
class ProductImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'width', 'height', 'display_width', 'display_hieght'], 'integer'],
            [['full_path'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
            [['mub_user_id'],'required'],
            [['full_path'],'required','on' => 'create'],
            [['visible', 'type', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['thumbnail_url', 'thumbnail_path'], 'string', 'max' => 100],
            [['keyword'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenarios['create'] = ['full_path','status','mub_user_id','name','created_at','slug'];
        $scenarios['update'] = ['full_path','name','mub_user_id','slug','updated_at'];
        return $scenario;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'mub_user_id' => 'Mub User ID',
            'thumbnail_url' => 'Thumbnail Url',
            'thumbnail_path' => 'Thumbnail Path',
            'full_path' => 'Full Path',
            'visible' => 'Visible',
            'keyword' => 'Keyword',
            'width' => 'Width',
            'height' => 'Height',
            'display_width' => 'Display Width',
            'display_hieght' => 'Display Hieght',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
