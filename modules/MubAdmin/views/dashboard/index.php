<?php

  use \app\models\MubUser;

?>
<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-server"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Capacity</p>
                                            105GB
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-wallet"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Revenue</p>
                                            $1,345
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="ti-calendar"></i> Last day
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-pulse"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Errors</p>
                                            23
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="ti-timer"></i> In the last hour
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-info text-center">
                                            <i class="ti-twitter-alt"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Followers</p>
                                            +45
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated now
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Users Behavior</h4>
                                <p class="category">24 Hours performance</p>
                            </div>
                            <div class="content">
                                <div id="chartHours" class="ct-chart"><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="245px" class="ct-chart-line" style="width: 100%; height: 245px;"><g class="ct-grids"><line y1="210" y2="210" x1="50" x2="915" class="ct-grid ct-vertical"></line><line y1="171" y2="171" x1="50" x2="915" class="ct-grid ct-vertical"></line><line y1="132" y2="132" x1="50" x2="915" class="ct-grid ct-vertical"></line><line y1="93" y2="93" x1="50" x2="915" class="ct-grid ct-vertical"></line><line y1="54" y2="54" x1="50" x2="915" class="ct-grid ct-vertical"></line><line y1="15" y2="15" x1="50" x2="915" class="ct-grid ct-vertical"></line></g><g><g class="ct-series ct-series-a"><path d="M50,210L50,154.035C86.042,154.035,122.083,134.925,158.125,134.925C194.167,134.925,230.208,114.45,266.25,114.45C302.292,114.45,338.333,100.41,374.375,100.41C410.417,100.41,446.458,94.17,482.5,94.17C518.542,94.17,554.583,87.93,590.625,87.93C626.667,87.93,662.708,73.89,698.75,73.89C734.792,73.89,770.833,35.475,806.875,35.475C842.917,35.475,878.958,24.36,915,24.36L915,210Z" class="ct-area" ct:values="[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object]"></path><path d="M50,154.035C86.042,154.035,122.083,134.925,158.125,134.925C194.167,134.925,230.208,114.45,266.25,114.45C302.292,114.45,338.333,100.41,374.375,100.41C410.417,100.41,446.458,94.17,482.5,94.17C518.542,94.17,554.583,87.93,590.625,87.93C626.667,87.93,662.708,73.89,698.75,73.89C734.792,73.89,770.833,35.475,806.875,35.475C842.917,35.475,878.958,24.36,915,24.36" class="ct-line"></path></g><g class="ct-series ct-series-b"><path d="M50,210L50,196.935C86.042,196.935,122.083,180.36,158.125,180.36C194.167,180.36,230.208,172.365,266.25,172.365C302.292,172.365,338.333,163.2,374.375,163.2C410.417,163.2,446.458,134.535,482.5,134.535C518.542,134.535,554.583,125.175,590.625,125.175C626.667,125.175,662.708,105.675,698.75,105.675C734.792,105.675,770.833,84.81,806.875,84.81C842.917,84.81,878.958,64.92,915,64.92L915,210Z" class="ct-area" ct:values="[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object]"></path><path d="M50,196.935C86.042,196.935,122.083,180.36,158.125,180.36C194.167,180.36,230.208,172.365,266.25,172.365C302.292,172.365,338.333,163.2,374.375,163.2C410.417,163.2,446.458,134.535,482.5,134.535C518.542,134.535,554.583,125.175,590.625,125.175C626.667,125.175,662.708,105.675,698.75,105.675C734.792,105.675,770.833,84.81,806.875,84.81C842.917,84.81,878.958,64.92,915,64.92" class="ct-line"></path></g><g class="ct-series ct-series-c"><path d="M50,210L50,205.515C86.042,205.515,122.083,187.965,158.125,187.965C194.167,187.965,230.208,196.935,266.25,196.935C302.292,196.935,338.333,188.94,374.375,188.94C410.417,188.94,446.458,172.95,482.5,172.95C518.542,172.95,554.583,163.395,590.625,163.395C626.667,163.395,662.708,150.135,698.75,150.135C734.792,150.135,770.833,130.05,806.875,130.05C842.917,130.05,878.958,130.05,915,130.05L915,210Z" class="ct-area" ct:values="[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object]"></path><path d="M50,205.515C86.042,205.515,122.083,187.965,158.125,187.965C194.167,187.965,230.208,196.935,266.25,196.935C302.292,196.935,338.333,188.94,374.375,188.94C410.417,188.94,446.458,172.95,482.5,172.95C518.542,172.95,554.583,163.395,590.625,163.395C626.667,163.395,662.708,150.135,698.75,150.135C734.792,150.135,770.833,130.05,806.875,130.05C842.917,130.05,878.958,130.05,915,130.05" class="ct-line"></path></g></g><g class="ct-labels"><foreignObject style="overflow: visible;" x="50" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">9:00AM</span></foreignObject><foreignObject style="overflow: visible;" x="158.125" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">12:00AM</span></foreignObject><foreignObject style="overflow: visible;" x="266.25" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">3:00PM</span></foreignObject><foreignObject style="overflow: visible;" x="374.375" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">6:00PM</span></foreignObject><foreignObject style="overflow: visible;" x="482.5" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">9:00PM</span></foreignObject><foreignObject style="overflow: visible;" x="590.625" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">12:00PM</span></foreignObject><foreignObject style="overflow: visible;" x="698.75" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">3:00AM</span></foreignObject><foreignObject style="overflow: visible;" x="806.875" y="215" width="108.125" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 108px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">6:00AM</span></foreignObject><foreignObject style="overflow: visible;" y="171" x="10" height="39" width="30"><span class="ct-label ct-vertical ct-start" style="height: 39px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">0</span></foreignObject><foreignObject style="overflow: visible;" y="132" x="10" height="39" width="30"><span class="ct-label ct-vertical ct-start" style="height: 39px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">200</span></foreignObject><foreignObject style="overflow: visible;" y="93" x="10" height="39" width="30"><span class="ct-label ct-vertical ct-start" style="height: 39px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">400</span></foreignObject><foreignObject style="overflow: visible;" y="54" x="10" height="39" width="30"><span class="ct-label ct-vertical ct-start" style="height: 39px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">600</span></foreignObject><foreignObject style="overflow: visible;" y="15" x="10" height="39" width="30"><span class="ct-label ct-vertical ct-start" style="height: 39px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">800</span></foreignObject><foreignObject style="overflow: visible;" y="-15" x="10" height="30" width="30"><span class="ct-label ct-vertical ct-start" style="height: 30px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">1000</span></foreignObject></g></svg></div>
                                <div class="footer">
                                    <div class="chart-legend">
                                        <i class="fa fa-circle text-info"></i> Open
                                        <i class="fa fa-circle text-danger"></i> Click
                                        <i class="fa fa-circle text-warning"></i> Click Second Time
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="ti-reload"></i> Updated 3 minutes ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>