<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163451_create_item_category extends Migration
{
    public function getTableName()
    {
        return 'item_category';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'name'  =>  'name',
            'status' => 'status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => "enum('shiping','item','planning') NOT NULL DEFAULT 'item'",
            'shiping_value' => "enum('value 1','value 2','value 3', 'Value 4', 'Value 5', 'Value 6') NOT NULL DEFAULT 'Value 1'",
            'plannig_value' => $this->string(),
            'slug' => $this->string()->notNull(),
            'priority' => $this->string(),
            'description' => $this->text(),
            'image' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['name','status','del_status'];
        $this->db->createCommand()->createIndex('unique_name_status', $this->getTableName(), $columns, true)->execute();
    }
}
