<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_114016_create_item extends Migration
{
    public function getTableName()
    {
        return 'item';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id' => 'mub_user_id',
            'name'  => 'name',
            'status' => 'status',                                      
            'created_at' => 'created_at'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'model_number' => $this->string(),
            'item_package_quantity' => "enum('quantity','quantity2') NOT NULL DEFAULT 'quantity'",
            'size' => $this->string(),
            'size_map' => $this->string(),
            'color' => $this->string(),
            'color_map' => $this->string(),
            'enclosure_material' => $this->string(),
            'manufacturer' => $this->integer(),
            'title' => $this->string(),
            'manufacturer_part_number' => $this->integer(),
            'brand' => $this->integer(),
            'category' => $this->string()->notNull(),
            'sub_category' => $this->string()->notNull(),
            'seller_sku' => $this->string(),
            'HSN_code' => $this->string(),
            'price' => $this->integer(),
            'sale_price' => $this->integer(),
            'warranty_des' => $this->string(),
            'country' => $this->string(),
            'release_date' => $this->string(),
            'sale_start_date' => $this->string(),
            'sale_end_date' => $this->string(),
            'restock_date' => $this->string(),
            'offer_release_date' => $this->string(),
            'condition_notes' => $this->string(),
            'leagle_notes' => $this->string(),
            'quantity' => $this->integer(),
            'max_order_quantity' => $this->integer(),
            'product_text_code' => $this->string(),
            'condition' => $this->string(),
            'handling_time' => $this->string(),
            'gift_wrap' => "enum('yes','no') NOT NULL DEFAULT 'no'",
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
