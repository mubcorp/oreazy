<?php
namespace app\migrations;
use app\commands\Migration;

/**
 * Class m180215_103104_sub_category
 */
class m180215_103104_item_sub_category extends Migration
{
    public function getTableName()
    {
        return 'item_sub_category';
    }
    public function getForeignKeyFields()
    {
       return [
            'item_category_id' => ['item_category','id'],
            'mub_user_id' => ['mub_user', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'name'  =>  'name',
            'status' => 'status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'item_category_id' => $this->integer()->notNull(),
            'mub_user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'priority' => $this->string(),
            'description' => $this->text(),
            'image' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['name','status','del_status'];
        $this->db->createCommand()->createIndex('unique_name_status', $this->getTableName(), $columns, true)->execute();
    }
}
