<?php

namespace app\migrations;
use app\commands\Migration;
/**
 * Class m180219_114439_brand
 */
class m180219_114439_brand extends Migration
{
    public function getTableName()
    {
        return 'brand';
    }
    public function getForeignKeyFields()
    {
       return [
            'manufacture_id' => ['manufacture','id'],
            'mub_user_id' => ['mub_user', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'name'  =>  'name',
            'logo' => 'logo',
            'status' => 'status',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'manufacture_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'logo' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $columns = ['name','status','del_status'];
        $this->db->createCommand()->createIndex('unique_name_status', $this->getTableName(), $columns, true)->execute();
    }
}
