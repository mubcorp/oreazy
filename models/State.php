<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property integer $id
 * @property string $state_name
 * @property string $active
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property City[] $cities
 * @property Property10[] $property10s
 * @property Property13[] $property13s
 */
class State extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active'], 'required'],
            [['active', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['state_name','state_slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state_name' => 'State Name',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertys()
    {
        return $this->hasMany(Property::className(), ['state_id' => 'id'])->where(['del_status' => '0']);
    }
}
