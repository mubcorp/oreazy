<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_detail".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $post_content
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Post $post
 */
class PostDetail extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_detail';
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id','read_count'], 'integer'],
            [['post_content'], 'required'],
            [['post_content', 'del_status'], 'string'],
            [['updated_at'], 'safe'],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create_post'] = ['post_content'];
        $scenarios['update_post'] = ['post_content'];
        return $scenarios;
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'post_content' => Yii::t('app', 'Post Content'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id'])->where(['del_status' => '0']);
    }
}
