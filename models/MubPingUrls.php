<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_ping_urls".
 *
 * @property integer $id
 * @property integer $url
 * @property integer $for_category
 * @property string $ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubCategory $forCategory
 * @property PostPingStats[] $postPingStats
 */
class MubPingUrls extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_ping_urls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'for_category'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['ip'], 'string', 'max' => 255],
            [['for_category'], 'exist', 'skipOnError' => true, 'targetClass' => MubCategory::className(), 'targetAttribute' => ['for_category' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'for_category' => Yii::t('app', 'For Category'),
            'ip' => Yii::t('app', 'Ip'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForCategory()
    {
        return $this->hasOne(MubCategory::className(), ['id' => 'for_category'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostPingStats()
    {
        return $this->hasMany(PostPingStats::className(), ['ping_id' => 'id'])->where(['del_status' => '0']);
    }
}
